from django.contrib import admin
from .models import Article, Category
from django.utils.html import format_html

# Admin header Change

admin.site.site_header = "پنل ادمین"


# Register your models here.

def make_pulished(modeladmin, request, queryset):
    rows_updated = queryset.update(status="p")
    if rows_updated == 1:
        message_bit = "منتشر شد ."
    else:
        message_bit = "منتشر شدند ."
    
    modeladmin.message_user(request, f"{rows_updated} مقاله {message_bit}")
make_pulished.short_description = "انتشار مقالات انتخاب شده"

def make_draft(modeladmin, request, queryset):
    rows_updated = queryset.update(status="d")
    if rows_updated == 1:
        message_bit = "پیش نویس شد ."
    else:
        message_bit = "پیش نویس شدند ."
    
    modeladmin.message_user(request, f"{rows_updated} مقاله {message_bit}")
make_draft.short_description = "پیش نویس شدن مقالات انتخاب شده"


class CategoryAdmin(admin.ModelAdmin):
    list_display = ("position", "title", "slug", "parent", "status")
    list_filter = (["status"])
    search_fields = ("title", "slug")
    prepopulated_fields = {"slug":("title",)}

admin.site.register(Category, CategoryAdmin)

class ArticleAdmin(admin.ModelAdmin):
    list_display = ("title", "thumbnail_tag", "slug", "jpublish", "status", "category_to_string")
    list_filter = ("publish", "status")
    search_fields = ("title", "description")
    prepopulated_fields = {"slug":("title",)}
    ordering = ("-status", "-publish")
    actions = [make_pulished, make_draft]

    def category_to_string(self, obj):
        return ",".join([category.title for category in obj.category_published()])
    category_to_string.short_description  = "دسته بندی"

admin.site.register(Article, ArticleAdmin)