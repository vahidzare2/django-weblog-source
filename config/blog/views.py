from django.shortcuts import render, get_object_or_404
from .models import Article, Category
from django.core.paginator import Paginator

from django.views.generic import ListView

# Create your views here.

#########################################################################

def home(request, page=1):
    articles_list = Article.objects.filter(status="p")
    paginator = Paginator(articles_list, 2)
    articles = paginator.get_page(page)
    context = {
        "articles": articles,
    }

    return render(request, "blog/index.html", context)


# class ArticleList(ListView):
#     queryset = Article.objects.filter(status="p")

#########################################################################

def detail(request, slug):
    context = {
        "article": get_object_or_404(Article, slug=slug, status="p"),
    }

    return render(request, "blog/post.html", context)

def category(request, slug, page=1):
    category = get_object_or_404(Category, slug=slug, status=True)
    articles_list = category.articles.published()
    paginator = Paginator(articles_list, 2)
    articles = paginator.get_page(page)
    context = {
        "articles": articles,
        "category": category
    }

    return render(request, "blog/category.html", context)