from django import template
from blog.models import Category

register = template.Library()

@register.simple_tag
def title():
    return "وبلاگ وحید زارع"

@register.inclusion_tag("blog/partials/category_navbar.html")
def category_navbar():
    context = {
        "categores": Category.objects.filter(status=True)
    }
    return context