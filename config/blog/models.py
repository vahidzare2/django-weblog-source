from django.db import models
from django.utils import timezone
from django.utils.html import format_html
from extensions.utils import jalali_converter

# my managers
class ArticleManager(models.Manager):
    def published(self):
        return self.filter(status="p")

class CategoryManager(models.Manager):
    def active(self):
        return self.filter(status=True)


# Create your models here.

class Category(models.Model):
    parent = models.ForeignKey('self', default=None, null=True, blank=True, on_delete=models.SET_NULL, related_name='children', verbose_name="زیر دسته")
    title = models.CharField(max_length=200, verbose_name="عنوان دسته بندی")
    slug = models.SlugField(max_length=100, unique=True, allow_unicode=True, verbose_name="لینک کوتاه")
    status = models.BooleanField(default=False, verbose_name='آیا نمایش داده شود ؟')
    position = models.IntegerField(verbose_name="پوزیشن")

    class Meta:
        verbose_name = "دسته بندی"
        verbose_name_plural = "دسته بندی ها"
        ordering = ["parent__id", "position"]

    def __str__(self):
        return self.title

    objects = CategoryManager()

class Article(models.Model):
    STATUS_CHOICE = (
        ('d', 'پیش نویس'),
        ('p', 'منتشر شده')
    )
    title = models.CharField(max_length=200, verbose_name="عنوان")
    slug = models.SlugField(max_length=100, unique=True, allow_unicode=True, verbose_name="لینک کوتاه")
    category = models.ManyToManyField(Category, verbose_name="دسته بندی", related_name="articles")
    description = models.TextField(verbose_name="متن وبلاگ")
    thumbnail = models.ImageField(upload_to="images", verbose_name="عکس")
    publish = models.DateTimeField(default=timezone.now, verbose_name="زمان و تاریخ انتشار")
    created = models.DateTimeField(auto_now_add=True, verbose_name="زمان و تاریخ ساخت")
    updated = models.DateTimeField(auto_now=True, verbose_name="زمان و تاریخ آپدیت")
    status = models.CharField(max_length=1, choices=STATUS_CHOICE, default='d', verbose_name="وضعیت انتشار")

    class Meta:
        verbose_name = "مقاله"
        verbose_name_plural = "مقالات"
        ordering = ["-publish"]

    def __str__(self):
        return self.title

    def jpublish(self):
        return jalali_converter(self.publish)

    jpublish.short_description = "زمان انتشار"

    def category_published(self):
        return self.category.filter(status=True)

    def thumbnail_tag(self):
        return format_html("<img width=100 height=75 style='border-radius:5px;' src='{}'>".format(self.thumbnail.url))
    thumbnail_tag.short_description = "تصویر"
    objects = ArticleManager()